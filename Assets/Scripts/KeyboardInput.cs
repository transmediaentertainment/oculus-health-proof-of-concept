﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class KeyboardInput : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
#if UNITY_EDITOR
            if (Application.isEditor)
            {
                EditorApplication.isPlaying = false;
            }
#endif
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.F1) && Application.loadedLevelName == "MusclesScene")
        {
            Application.LoadLevel("room_example");
        }
	}
}
