﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using System;

public enum CamRaycastResult
{
    Nothing,
    BPI,
    TPO
}

public class CameraController : MonoBehaviour
{
    private float m_LookTimer = 0f;
    private const float m_MoveLookTime = 1.5f;
    private Collider m_LookTarget;
    private const float m_TotalMoveTime = 2f;
    public Transform m_RightCam;
    public Transform m_LeftCam;
    private Tweener m_Tweener;
    private BodyPartInformation m_LastBPI;

    public static event Action<CamRaycastResult, MonoBehaviour> RayCastResultEvent;

    public UIController m_UIController;

    void Update()
    {
        if (m_UIController.InstructionsShowing)
            return;
        // If we aren't moving
        if (m_Tweener == null)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, CamForward(), out hitInfo, 250f))
            {
                var tpo = hitInfo.collider.GetComponent<TargetPositionObject>();
                var bpi = hitInfo.collider.GetComponent<BodyPartInformation>();
                if (tpo != null)
                {
                    tpo.SetTargeted();
                    if (m_LastBPI != null)
                    {
                        m_LastBPI.TurnOffHighlight();
                        m_LastBPI = null;
                    }
                    if (m_LookTarget == hitInfo.collider)
                    {
                        m_LookTimer += Time.deltaTime;
                        if (m_LookTimer >= m_MoveLookTime)
                        {
                            m_Tweener = HOTween.To(transform, m_TotalMoveTime, new TweenParms().Prop("position", m_LookTarget.transform.position).Ease(EaseType.EaseInOutCubic).OnComplete(() => { m_Tweener = null; }));
                            m_LookTimer = 0f;
                        }
                    }
                    else
                    {
                        // Reset timer and set new look target
                        m_LookTimer = 0f;
                        m_LookTarget = hitInfo.collider;
                    }
                    FireRaycastEvent(CamRaycastResult.TPO, tpo);
                }
                else if (bpi != null)
                {
                    if (m_LastBPI != null)
                    {
                        if (m_LastBPI != bpi)
                        {
                            m_LastBPI.TurnOffHighlight();
                        }
                    }
                    m_LastBPI = bpi;
                    m_LastBPI.TurnOnHighlight();
                    m_LookTimer = 0f;
                    FireRaycastEvent(CamRaycastResult.BPI, bpi);
                }
                else
                {
                    FireRaycastEvent(CamRaycastResult.Nothing, null);
                }
            }
            else
            {
                m_LookTimer = 0f;
                if (m_LastBPI != null)
                {
                    m_LastBPI.TurnOffHighlight();
                    m_LastBPI = null;
                }
                FireRaycastEvent(CamRaycastResult.Nothing, null);
            }
        }
        else
        {
            if (m_LastBPI != null)
            {
                m_LastBPI.TurnOffHighlight();
                m_LastBPI = null;
            }
            FireRaycastEvent(CamRaycastResult.Nothing, null);
        }
    }

    private void FireRaycastEvent(CamRaycastResult result, MonoBehaviour behaviour)
    {
        if (RayCastResultEvent != null)
        {
            RayCastResultEvent(result, behaviour);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + CamForward() * 250f);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(m_LeftCam.position, m_LeftCam.position + m_LeftCam.forward * 250f);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(m_RightCam.position, m_RightCam.position + m_RightCam.forward * 250f);
    }

    private Vector3 CamForward()
    {
        return (m_LeftCam.forward + m_RightCam.forward) * 0.5f;
    }
}
