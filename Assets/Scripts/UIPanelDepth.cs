﻿using UnityEngine;
using System.Collections;

public class UIPanelDepth : MonoBehaviour 
{
    private float m_Speed = 25f;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.P))
        {
            var pos = transform.localPosition;
            pos.z += m_Speed * Time.deltaTime;
            transform.localPosition = pos;
        }
        if (Input.GetKey(KeyCode.O))
        {
            
            var pos = transform.localPosition;
            pos.z -= m_Speed * Time.deltaTime;
            transform.localPosition = pos;
        }

	}
}
