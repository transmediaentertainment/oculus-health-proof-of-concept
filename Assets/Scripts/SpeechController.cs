﻿using UnityEngine;
using System.Collections;
using PathologicalGames;
using Holoville.HOTween;
using System;

public class SpeechController : MonoBehaviour
{
    public Transform m_AudioPrefab;
    private GameObject m_MainSource;
    private BodyPartInformation m_BPI;
    private int m_CurrentSpeechPart = 0;
    private int m_CurrentChunk;
    private float m_CurrentSpeechLength;
    private float m_OverallTimer;
    private float m_ChunkTimer;

    public event Action<string> NewChunkEvent;

    void Awake()
    {
        UIController.Instance.ScrollingBeganEvent += BeginPlaying;
        UIController.Instance.ScrollingEndedEvent += StopPlaying;
    }

    private void BeginPlaying(BodyPartInformation bpi)
    {
        if (m_BPI == bpi && m_MainSource != null)
        {
            Debug.Log("Already playing");
            return;
        }
        Debug.Log("Playing New");
        m_BPI = bpi;
        WindDownMainSource();
        m_MainSource = PoolManager.Pools["SpeechPool"].Spawn(m_AudioPrefab).gameObject;
        m_MainSource.GetComponent<AudioSource>().clip = m_BPI.m_Clip1;
        m_CurrentSpeechPart = 1;
        m_OverallTimer = 0f;
        m_ChunkTimer = 0f;
        m_CurrentChunk = 0;
        m_MainSource.GetComponent<AudioSource>().volume = 1f;
        m_CurrentSpeechLength = m_BPI.m_Clip1.length;
        m_MainSource.GetComponent<AudioSource>().Play();
        if (NewChunkEvent != null)
        {
            if(m_BPI.m_Clip1Sections.Length > 0)
            {
                NewChunkEvent(m_BPI.m_Clip1Sections[0].Text);
            }
        }
    }

    private void StopPlaying()
    {
        WindDownMainSource();
        m_CurrentSpeechPart = 0;
    }

    private void WindDownMainSource()
    {
        if (m_MainSource != null)
        {
            var windDownSource = m_MainSource;
            m_MainSource = null;
            HOTween.To(windDownSource.GetComponent<AudioSource>(), 0.25f, new TweenParms().Prop("volume", 0f)
                .OnComplete(() =>
                {
                    PoolManager.Pools["SpeechPool"].Despawn(windDownSource.transform);
                }));
        }
    }

    private float GetLimit(SpeechSection[] sections, int currentChunk)
    {
        if (currentChunk < sections.Length)
        {
            return sections[currentChunk].Time;
        }
        return 1000f;
    }

    private string GetText(SpeechSection[] sections, int currentChunk)
    {
        if (currentChunk < sections.Length)
            return sections[currentChunk].Text;
        return "";
    }

    void Update()
    {
        if (m_CurrentSpeechPart > 0 && m_CurrentSpeechPart < 4)
        {
            m_OverallTimer += Time.deltaTime;
            m_ChunkTimer += Time.deltaTime;
            // Chunk
            bool nextChunk = false;
            float limit = 1000f;
            switch(m_CurrentSpeechPart)
            {
                case 1:
                    limit = GetLimit(m_BPI.m_Clip1Sections, m_CurrentChunk);
                    break;
                case 2:
                    limit = GetLimit(m_BPI.m_Clip2Sections, m_CurrentChunk);
                    break;
                case 3:
                    limit = GetLimit(m_BPI.m_Clip3Sections, m_CurrentChunk);
                    break;
            }
            if (m_ChunkTimer >= limit)
            {
                m_ChunkTimer -= limit;
                ++m_CurrentChunk;
                string newText = "";

                switch (m_CurrentSpeechPart)
                {
                    case 1:
                        newText = GetText(m_BPI.m_Clip1Sections, m_CurrentChunk);
                        break;
                    case 2:
                        newText = GetText(m_BPI.m_Clip2Sections, m_CurrentChunk);
                        break;
                    case 3:
                        newText = GetText(m_BPI.m_Clip3Sections, m_CurrentChunk);
                        break;
                }
                if (!string.IsNullOrEmpty(newText))
                {
                    if (NewChunkEvent != null)
                        NewChunkEvent(newText);
                }
            }

            // Overall
            if (m_OverallTimer >= m_CurrentSpeechLength)
            {
                Debug.Log("Moving Chunk");
                ++m_CurrentSpeechPart;
                if (m_CurrentSpeechPart >= 4)
                {
                    // Loop
                    //m_CurrentSpeechPart = 1;
                    // Don't loop, just finish.
                    if (NewChunkEvent != null)
                        NewChunkEvent("");
                    return;
                }
                string newText = "";
                switch (m_CurrentSpeechPart)
                {
                    case 1:
                        m_MainSource.GetComponent<AudioSource>().clip = m_BPI.m_Clip1;
                        newText = GetText(m_BPI.m_Clip1Sections, 0);
                        break;
                    case 2:
                        m_MainSource.GetComponent<AudioSource>().clip = m_BPI.m_Clip2;
                        newText = GetText(m_BPI.m_Clip2Sections, 0);
                        break;
                    case 3:
                        m_MainSource.GetComponent<AudioSource>().clip = m_BPI.m_Clip3;
                        newText = GetText(m_BPI.m_Clip3Sections, 0);
                        break;
                }
                m_CurrentSpeechLength = m_MainSource.GetComponent<AudioSource>().clip.length;
                m_OverallTimer = 0f;
                m_MainSource.GetComponent<AudioSource>().Play();
                m_CurrentChunk = 0;
                m_ChunkTimer = 0f;
                if (NewChunkEvent != null)
                    NewChunkEvent(newText);

                Debug.Log("Moved to clip : " + m_CurrentSpeechPart);
            }
        }
    }
}
