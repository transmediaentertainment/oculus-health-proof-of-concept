﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using PathologicalGames;

public class TargetPositionObject : MonoBehaviour
{
    private bool m_IsTargeted;
    private static bool m_TweenInit = false;
    private Tweener m_ScaleUpTweener;
    private Tweener m_ScaleDownTweener;
    public GameObject m_RadialPrefab;
    private GameObject m_RadialInstance;
    private RadialComponent m_RadialComponent;
    public Camera m_WorldCamera;

    void Awake()
    {
        if (!m_TweenInit)
        {
            m_TweenInit = true;
            HOTween.Init(false, false, true);
            HOTween.EnableOverwriteManager();
        }
    }

    public void SetTargeted()
    {
        m_IsTargeted = true;
        if (m_RadialInstance == null) 
        {
            var t = PoolManager.Pools["UIPool"].Spawn(m_RadialPrefab.transform);
            t.parent = transform;
            t.localPosition = Vector3.zero;
            m_RadialInstance = t.gameObject;
            m_RadialInstance.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            m_RadialComponent = m_RadialInstance.GetComponentInChildren<RadialComponent>();
            m_RadialComponent.Init(m_WorldCamera);
            m_RadialComponent.BecameEmptyEvent += RadialBecameEmpty;
        }
    }

    private void RadialBecameEmpty()
    {
        m_RadialComponent.BecameEmptyEvent -= RadialBecameEmpty;
        PoolManager.Pools["UIPool"].Despawn(m_RadialInstance.transform);

        m_RadialInstance = null;
        m_RadialComponent = null;
    }

    void LateUpdate()
    {
        if (m_RadialComponent != null)
        {
            if (m_IsTargeted && !m_RadialComponent.IsFull)
            {
                m_RadialComponent.BeginFilling();
            }
            else if (!m_IsTargeted)
            {
                m_RadialComponent.BeginEmptying();
            }
        }
        m_IsTargeted = false;
    }

}
