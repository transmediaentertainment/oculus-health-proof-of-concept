﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public abstract class RadialMovingState : FSMState 
{
    private float m_MovingRate;
    private UISprite m_Sprite;
    private float m_TestLimit;
    private Func<float, float, bool> m_EventTest;
    public event Action MovingCompletedEvent;

    public RadialMovingState(UISprite sprite, float moveRate, float testLimit, Func<float, float, bool> eventTest)
    {
        m_MovingRate = moveRate;
        m_Sprite = sprite;
        m_EventTest = eventTest;
        m_TestLimit = testLimit;
    }

    public override void Update(float deltaTime)
    {
        var fill = m_Sprite.fillAmount;
        fill += m_MovingRate * deltaTime;
        m_Sprite.fillAmount = Mathf.Clamp01(fill);
        if(m_EventTest(fill, m_TestLimit))
        {
            if (MovingCompletedEvent != null)
                MovingCompletedEvent();
        }
    }
}
