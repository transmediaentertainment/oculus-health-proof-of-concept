﻿using UnityEngine;
using System.Collections;
using System;

public class RadialEmptyingState : RadialMovingState
{
    public RadialEmptyingState(UISprite sprite, float moveRate)
        : base(sprite, -moveRate, 0f, EmptyTest) { }

    private static bool EmptyTest(float val, float limit)
    {
        return val <= limit;
    }
}
