﻿using UnityEngine;
using System.Collections;
using System;

public class RadialFillingState : RadialMovingState
{
    public RadialFillingState(UISprite sprite, float moveRate)
        : base(sprite, moveRate, 1f, FilledTest) { }

    private static bool FilledTest(float val, float limit)
    {
        return val >= limit;
    }
}
