﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public class RadialComponent : MonoBehaviour 
{
    private FSM m_FSM;
    private RadialFillingState m_FillingState;
    private RadialEmptyingState m_EmptyingState;
    private IdleFSMState m_IdleState;

    public UISprite m_Sprite;
    public float m_FillTime = 1.5f;
    public float m_EmptyTime = 0.5f;
    private bool m_IsFull = false;
    public bool IsFull { get { return m_IsFull; } }
    private Camera m_WorldCamera;
    public event Action BecameEmptyEvent;

    void Awake()
    {
        m_FSM = new FSM();
        m_IdleState = new IdleFSMState();
        m_FillingState = new RadialFillingState(m_Sprite, 1f / m_FillTime);
        m_EmptyingState = new RadialEmptyingState(m_Sprite, 1f / m_EmptyTime);

        m_Sprite.fillAmount = 0f;
        m_FSM.NewState(m_IdleState);
        m_FillingState.MovingCompletedEvent += SetFull;
        m_EmptyingState.MovingCompletedEvent += SetEmtpy;
    }

    public void Init(Camera worldCam)
    {
        m_WorldCamera = worldCam;
    }

    public void BeginFilling()
    {
        if (m_FSM.Peek() != m_FillingState)
        {
            m_FSM.NewState(m_FillingState);
        }
    }

    public void BeginEmptying()
    {
        if (m_FSM.Peek() != m_EmptyingState)
        {
            m_FSM.NewState(m_EmptyingState);
            m_IsFull = false;
        }
    }

    private void SetEmtpy()
    {
        m_IsFull = false;
        m_FSM.NewState(m_IdleState);
        if (BecameEmptyEvent != null)
            BecameEmptyEvent();
    }

    private void SetFull()
    {
        m_IsFull = true;
        m_FSM.NewState(m_IdleState);
    }

    void LateUpdate()
    {
        transform.LookAt(m_WorldCamera.transform);
    }
}
