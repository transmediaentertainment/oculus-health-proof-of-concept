﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class OpeningSceneBodyController : MonoBehaviour 
{
    public Transform m_RightCam;
    public Transform m_LeftCam;

    private RenderTexture m_RenderTexture;
    public Camera m_UICamera;
    public Renderer m_UIRenderer;
    public UIPanel m_UIPanel;

    private Tweener m_FadeInTweener;
    private Tweener m_FadeOutTweener;

    private void Awake()
    {
        var resolution = Screen.currentResolution;
        m_RenderTexture = new RenderTexture(resolution.width, resolution.height, 24, RenderTextureFormat.Default);
        m_RenderTexture.Create();
        m_UICamera.targetTexture = m_RenderTexture;

        var mat = new Material(Shader.Find("Unlit/Transparent Colored On Top"));
        mat.mainTexture = m_RenderTexture;
        m_UIRenderer.material = mat;

        m_UIPanel.alpha = 0f;
    }

    void Update()
    {
        RaycastHit hitInfo;
        bool fadeDown = true;
        if (Physics.Raycast(m_RightCam.position, CamForward(), out hitInfo, 2f))
        {
            // If the raycast hit us
            if (hitInfo.collider == GetComponent<Collider>())
            {
                fadeDown = false;
                if (m_FadeInTweener == null)
                {
                    m_FadeOutTweener = null;
                    m_FadeInTweener = HOTween.To(m_UIPanel, 0.25f, new TweenParms().Prop("alpha", 1f)
                        .OnComplete(() => {m_FadeInTweener = null;}));
                }

                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("Loading");

                    Application.LoadLevel("MusclesScene");
                }
            }
        }
        if(fadeDown)
        {
            if(m_FadeOutTweener == null)
            {
                m_FadeInTweener = null;
                m_FadeOutTweener = HOTween.To(m_UIPanel, 0.25f, new TweenParms().Prop("alpha", 0f)
                    .OnComplete(() => {m_FadeOutTweener = null;}));
            }
        }
    }

    private Vector3 CamForward()
    {
        return (m_LeftCam.forward + m_RightCam.forward) * 0.5f;
    }
}
