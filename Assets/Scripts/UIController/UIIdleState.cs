﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public class UIIdleState : UIFSMState
{
    private BodyPartInformation m_CurrentLookingBPI;
    private float m_RaiseTimer;
    private float m_RaiseLimit;
    private UIController m_UIController;

    public event Action<BodyPartInformation> RaiseTimerLimitEvent;

    public UIIdleState(float raiseLimit, UIController uiController)
    {
        m_RaiseLimit = raiseLimit;
        m_UIController = uiController;
    }
   
    protected override void HandleRaycastEvent(CamRaycastResult result, MonoBehaviour behaviour)
    {
        if (m_UIController.InstructionsShowing)
            return;
        switch (result)
        {
            case CamRaycastResult.Nothing:
                m_RaiseTimer = 0f;
                m_CurrentLookingBPI = null;
                m_UIController.HideUI();
                break;
            case CamRaycastResult.BPI:
                var bpi = (BodyPartInformation)behaviour;
                if (bpi != m_CurrentLookingBPI)
                {
                    m_CurrentLookingBPI = bpi;
                    m_RaiseTimer = 0f;
                }
                m_RaiseTimer += Time.deltaTime;
                m_UIController.ShowUI();
                if(m_RaiseTimer >= m_RaiseLimit)
                {
                    if (RaiseTimerLimitEvent != null)
                        RaiseTimerLimitEvent(m_CurrentLookingBPI);
                }
                break;
            case CamRaycastResult.TPO:
                m_RaiseTimer = 0f;
                m_CurrentLookingBPI = null;
                m_UIController.HideUI();
                break;
        }
    }
}
