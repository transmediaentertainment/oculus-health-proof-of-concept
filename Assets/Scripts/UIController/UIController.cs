﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Plugins;
using System;
using TransTech.FiniteStateMachine;

public class UIController :  MonoSingleton<UIController>
{
    public SpeechController m_SpeechController;
    public Camera m_UICamera;
    private RenderTexture m_RenderTexture;
    public Renderer m_UIRenderer;
    private BodyPartInformation m_CurrentBPI;
    public UIPanel m_Panel1;
    public UILabel m_Label1;

    public UIPanel m_Panel2;
    public UIPanel m_ClipPanel2;
    public UILabel m_Label2Name;
    public UILabel m_Label2ScrollText;

    public UIPanel m_Panel3;
    public UIPanel m_ClipPanel3;
    public UILabel m_Label3Name;
    public UILabel m_Label3Text1;
    public UILabel m_Label3Text2;
    private bool m_Chunk1InUse = false;

    public UIPanel m_Panel4;
    public bool InstructionsShowing { get; private set; }
    private float m_InstructionsTimer = 0f;
    private float m_InstructionsLength = 12f;

    private Vector3 m_ChunkLabelLeftPos = new Vector3(-800f, 0f, 0f);
    private Vector3 m_ChunkLabelRightPos = new Vector3(800f, 0f, 0f);

    public UISprite m_NameChargeSprite;

    private int m_CurrentUIType = 1;
    private bool m_IsShowing = false;
    private float m_Label2Length;
    public float m_TextScrollSpeed = 5f;
    public float m_InitialRaiseTime = 0.5f;
    public float m_NameChargeTime = 1.5f;
    public float m_LookAwayTime = 2f;

    public Material[] m_MainMaterials;
    private Shader m_OpaqueMuscleShader;
    private Shader m_TransparentMuscleShader;
    private Tweener[] m_DimTweeners;
    private Tweener[] m_BrightTweeners;

    private FSM m_FSM;
    private UIIdleState m_IdleState;
    private TextAudioState m_TextAudioState;
    private NameChargeState m_NameChargeState;

    public event Action<BodyPartInformation> ScrollingBeganEvent;
    public event Action ScrollingEndedEvent;

    public override void Init()
    {
        base.Init();

		m_OpaqueMuscleShader = Shader.Find("Shader Forge/BumpedSpecularRimlight");
		m_TransparentMuscleShader = Shader.Find("Shader Forge/BumpedSpecularRimlightTransparent");
    
        var resolution = Screen.currentResolution;
        m_RenderTexture = new RenderTexture(resolution.width, resolution.height, 24, RenderTextureFormat.Default);
        m_RenderTexture.Create();
        m_UICamera.targetTexture = m_RenderTexture;

        var mat = new Material(Shader.Find("Unlit/Transparent Colored On Top"));
        mat.mainTexture = m_RenderTexture;
        m_UIRenderer.material = mat;

        m_CurrentUIType = 3;

        m_Panel1.alpha = 0f;
        m_Panel2.alpha = 0f;
        m_Panel3.alpha = 0f;
        m_Label2Name.alpha = 0f;
        m_Label2ScrollText.text = "";

        m_Label3Name.alpha = 0f;
        m_Label3Text1.text = "";
        m_Label3Text2.text = "";

        m_DimTweeners = new Tweener[m_MainMaterials.Length];
        m_BrightTweeners = new Tweener[m_MainMaterials.Length];

        m_IdleState = new UIIdleState(m_InitialRaiseTime, this);
        m_TextAudioState = new TextAudioState(m_LookAwayTime, this);
        m_NameChargeState = new NameChargeState(m_NameChargeTime, m_NameChargeSprite);
        m_FSM = new FSM();
        m_FSM.NewState(m_IdleState);

        m_IdleState.RaiseTimerLimitEvent += InitalRaiseTimeHandler;
        m_NameChargeState.ChargeCompletedEvent += NameChargeCompletedHandler;
        m_NameChargeState.ChargeFailedEvent += NameChargeFailedHandler;
        m_TextAudioState.LookAwayEvent += LookAwayHandler;

        m_SpeechController.NewChunkEvent += SetNewChunkText;

        m_Panel4.alpha = 1f;
        InstructionsShowing = true;
    }

    private void InitalRaiseTimeHandler(BodyPartInformation bpi)
    {
        if (m_CurrentBPI != bpi)
        {
            m_CurrentBPI = bpi;
            UpdateBPIReadout();
            SetUIToName();
        }
        HOTween.To(m_Label2Name, 0.25f, new TweenParms().Prop("alpha", 1f));
        HOTween.To(m_Label3Name, 0.25f, new TweenParms().Prop("alpha", 1f));
        m_FSM.NewState(m_NameChargeState, m_CurrentBPI);
    }

    private void NameChargeFailedHandler()
    {
        m_FSM.NewState(m_IdleState);
    }

    private void NameChargeCompletedHandler()
    {
        m_FSM.NewState(m_TextAudioState, m_CurrentBPI);
    }

    private void LookAwayHandler()
    {
        m_FSM.NewState(m_IdleState);
    }

    private void SetUIToName()
    {
        m_Label2Name.text = m_CurrentBPI.m_PartName;
        m_Label2Name.transform.localPosition = Vector3.zero;
        m_Label3Name.text = m_CurrentBPI.m_PartName;
        m_Label3Name.transform.localPosition = Vector3.zero;
    }

    public void ShowUI()
    {
        if (!m_IsShowing)
        {
            switch (m_CurrentUIType)
            {
                case 1:
                    m_Panel1.alpha = 1f;
                    m_Panel2.alpha = 0f;
                    m_Panel3.alpha = 0f;
                    break;
                case 2:
                    m_Panel1.alpha = 0f;
                    HOTween.To(m_Panel2, m_InitialRaiseTime, new TweenParms().Prop("alpha", 1f));
                    m_Label2Name.alpha = 0f;
                    break;
                case 3:
                    m_Panel1.alpha = 0f;
                    m_Panel2.alpha = 0f;
                    HOTween.To(m_Panel3, m_InitialRaiseTime, new TweenParms().Prop("alpha", 1f));
                    m_Label3Text1.transform.localPosition = m_ChunkLabelRightPos;
                    m_Label3Text2.transform.localPosition = m_ChunkLabelRightPos;
                    break;
            }
            m_IsShowing = true;
        }
    }

    public void HideUI()
    {
        if (m_IsShowing)
        {
            m_Panel1.alpha = 0f;
            HOTween.To(m_Panel2, m_InitialRaiseTime, new TweenParms().Prop("alpha", 0f));
            HOTween.To(m_Label2Name, 0.25f, new TweenParms().Prop("alpha", 0f));
            HOTween.To(m_Panel3, m_InitialRaiseTime, new TweenParms().Prop("alpha", 0f));
            m_IsShowing = false;
        }
    }

    /*public void SetBodyPartInfo(BodyPartInformation info)
    {
        if (m_CurrentBPI != info)
        {
            m_CurrentBPI = info;
            UpdateBPIReadout();
        }
    }*/

    private bool m_IsScrolling = false;
    public void BeginScrolling()
    {
        m_IsScrolling = true;
        HOTween.To(m_Label2Name, 0.25f, new TweenParms().Prop("alpha", 0f));
        HOTween.To(m_Label3Name, 0.25f, new TweenParms().Prop("alpha", 0f));
        if (ScrollingBeganEvent != null)
            ScrollingBeganEvent(m_CurrentBPI);
    }

    public void StopScrolling()
    {
        m_IsScrolling = false;
        // Reset Position
        m_Label2Length = m_Label2ScrollText.printedSize.x;
        var xPos = (m_Label2ScrollText.printedSize.x * 0.5f) + (m_ClipPanel2.GetViewSize().x * 0.5f);
        m_Label2ScrollText.transform.localPosition = new Vector3(xPos, 0f, 0f);
        if (ScrollingEndedEvent != null)
            ScrollingEndedEvent();
    }

    private void UpdateBPIReadout()
    {
        m_Label1.text = m_CurrentBPI.m_PartName + "\nOrigin: " + m_CurrentBPI.m_Origin + "\nInsertion: " + m_CurrentBPI.m_Insertion
            + "\nFunction: " + m_CurrentBPI.m_Function + "\nInnervation: " + m_CurrentBPI.m_Innervation;
        var text = m_CurrentBPI.m_PartName + "   Origin: " + m_CurrentBPI.m_Origin + "   Insertion: " + m_CurrentBPI.m_Insertion
            + "   Function: " + m_CurrentBPI.m_Function + "   Innervation: " + m_CurrentBPI.m_Innervation;
        text = text.Replace("\n", "   ");
        m_Label2ScrollText.text = text;

        m_Label2Length = m_Label2ScrollText.printedSize.x;
        var xPos = (m_Label2ScrollText.printedSize.x * 0.5f) + (m_ClipPanel2.GetViewSize().x * 0.5f);
        m_Label2ScrollText.transform.localPosition = new Vector3(xPos, 0f, 0f);

    }

    void Update()
    {
        if (InstructionsShowing)
        {
            m_InstructionsTimer += Time.deltaTime;
            if (m_InstructionsTimer >= m_InstructionsLength)
            {
                HOTween.To(m_Panel4, 0.25f, new TweenParms().Prop("alpha", 0f));
                InstructionsShowing = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            m_CurrentUIType = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            m_CurrentUIType = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
        {
            m_CurrentUIType = 3;
        }
        if (m_IsScrolling && m_CurrentUIType == 2)
        {
            var pos = m_Label2ScrollText.transform.localPosition;
            pos.x -= m_TextScrollSpeed * Time.deltaTime;
            m_Label2ScrollText.transform.localPosition = pos;
            // Check if we need to loop.
            if (pos.x <= -(m_Label2Length * 0.5f) - (m_ClipPanel2.GetViewSize().x * 0.5f))
            {
                var xPos = (m_Label2ScrollText.printedSize.x * 0.5f) + (m_ClipPanel2.GetViewSize().x * 0.5f);
                m_Label2ScrollText.transform.localPosition = new Vector3(xPos, 0f, 0f);
            }
        }
    }

    private void SetNewChunkText(string newText)
    {
        var moveOffLabel = m_Chunk1InUse ? m_Label3Text1 : m_Label3Text2;
        var moveOnLabel = m_Chunk1InUse ? m_Label3Text2 : m_Label3Text1;

        moveOnLabel.text = newText;
        moveOnLabel.transform.localPosition = m_ChunkLabelRightPos;
        HOTween.To(moveOnLabel.transform, 0.25f, new TweenParms().Prop("localPosition", Vector3.zero).Ease(EaseType.EaseInOutCirc));

        HOTween.To(moveOffLabel.transform, 0.25f, new TweenParms().Prop("localPosition", m_ChunkLabelLeftPos).Ease(EaseType.EaseInOutCirc));

        m_Chunk1InUse = !m_Chunk1InUse;
    }

    public void DimMainMaterials()
    {
        if (m_DimTweeners[0] == null)
        {
            for (int i = 0; i < m_BrightTweeners.Length; i++)
            {
                if (m_BrightTweeners[i] != null)
                {
                    m_BrightTweeners[i].Kill();
                    m_BrightTweeners[i] = null;
                }
            }
            for (int i = 0; i < m_MainMaterials.Length; i++)
            {
                var index = i;
                // Switch to the transparent Material
                m_MainMaterials[i].shader = m_TransparentMuscleShader;
                m_DimTweeners[i] = HOTween.To(m_MainMaterials[i], 0.3f, new TweenParms()
                    .Prop("shader", new PlugSetColor(new Color(0f, 0f, 0f, 0f)).Property("_Color"))
                    .OnComplete(() => {
                        m_DimTweeners[index] = null;
                    }));
                //m_MainMaterials[i].SetColor("_ColorTint", new Color(0.25f, 0.25f, 0.25f));
            }
        }
    }

    public void BrightenMainMainterials()
    {
        if (m_BrightTweeners[0] == null)
        {
            for (int i = 0; i < m_DimTweeners.Length; i++)
            {
                if (m_DimTweeners[i] != null)
                {
                    m_DimTweeners[i].Kill();
                    m_DimTweeners[i] = null;
                }
            }
            for (int i = 0; i < m_MainMaterials.Length; i++)
            {
                var index = i;
                m_BrightTweeners[i] = HOTween.To(m_MainMaterials[i], 0.3f, new TweenParms()
                    .Prop("shader", new PlugSetColor(Color.white).Property("_Color"))
                    .OnComplete(() => {
                        m_BrightTweeners[index] = null;
                        m_MainMaterials[index].shader = m_OpaqueMuscleShader;
                    }));
                //m_MainMaterials[i].SetColor("_ColorTint", Color.white);
            }
        }
    }

    private void OnDestroy()
    {
        // Set the materials back
        for (int i = 0; i < m_MainMaterials.Length; i++)
        {
            m_MainMaterials[i].color = Color.white;
            m_MainMaterials[i].shader = m_OpaqueMuscleShader;
            //m_MainMaterials[i].SetColor("_ColorTint", Color.white);
        }
    }
}
