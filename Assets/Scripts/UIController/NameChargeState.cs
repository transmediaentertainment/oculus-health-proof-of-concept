﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;
using Holoville.HOTween;

public class NameChargeState : UIFSMState 
{
    private float m_ChargeTime;
    private float m_Timer;
    private UISprite m_ChargeSprite;
    private BodyPartInformation m_CurrentBPI;

    public event Action ChargeFailedEvent;
    public event Action ChargeCompletedEvent;

    private Tweener m_HideTweener;

    public NameChargeState(float nameChargeTime, UISprite chargeSprite)
    {
        m_ChargeTime = nameChargeTime;
        m_ChargeSprite = chargeSprite;
        m_ChargeSprite.fillAmount = 0f;
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);
        m_Timer = 0f;
        // Force reset
        if (m_HideTweener != null)
        {
            m_HideTweener.Kill();
            m_HideTweener = null;
        }
        m_ChargeSprite.fillAmount = 0f;
        if (args == null || args.Length != 1)
        {
            Debug.LogError("Incorrect arguments passed into NameChargeState Enter function");
        }
        else
        {
            m_CurrentBPI = (BodyPartInformation)(args[0]);
        }
    }

    protected override void HandleRaycastEvent(CamRaycastResult result, MonoBehaviour behaviour)
    {
        if (result == CamRaycastResult.BPI && (BodyPartInformation)behaviour == m_CurrentBPI)
        {
            m_Timer += Time.deltaTime;
            if (m_Timer >= m_ChargeTime)
            {
                if(ChargeCompletedEvent != null)
                    ChargeCompletedEvent();
            }
        }
        else
        {
            m_Timer -= Time.deltaTime;
            if (m_Timer <= 0f)
            {
                if (ChargeFailedEvent != null)
                    ChargeFailedEvent();
            }
        }
        m_ChargeSprite.fillAmount = Mathf.Clamp01(m_Timer / m_ChargeTime);
    }

    public override void Exit()
    {
        base.Exit();

        m_HideTweener = HOTween.To(m_ChargeSprite, 0.25f, new TweenParms().Prop("fillAmount", 0f)
            .OnComplete(() => {
                m_HideTweener = null;
            }));
    }
}
