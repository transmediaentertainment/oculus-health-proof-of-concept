﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public abstract class UIFSMState : FSMState 
{

	public override void Enter(params object[] args)
    {
        base.Enter(args);
        CameraController.RayCastResultEvent += HandleRaycastEvent;
    }

    public override void Exit()
    {
        base.Exit();
        CameraController.RayCastResultEvent -= HandleRaycastEvent;
    }

    protected abstract void HandleRaycastEvent(CamRaycastResult result, MonoBehaviour behaviour);
}
