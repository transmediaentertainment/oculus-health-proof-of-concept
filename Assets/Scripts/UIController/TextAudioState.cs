﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public class TextAudioState : UIFSMState 
{
    private float m_LookAwayTime;
    private float m_Timer;
    private BodyPartInformation m_CurrentBPI;
    private UIController m_UIController;

    public event Action LookAwayEvent;

    public TextAudioState(float lookAwayTime, UIController uiController)
    {
        m_LookAwayTime = lookAwayTime;
        m_UIController = uiController;
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);
        m_Timer = 0f;
        if (args == null || args.Length != 1)
        {
            Debug.LogError("Incorrect arguments passed into TextAudioState Enter function");
        }
        else
        {
            m_CurrentBPI = (BodyPartInformation)(args[0]);
        }
        m_UIController.BeginScrolling();
    }

    protected override void HandleRaycastEvent(CamRaycastResult result, MonoBehaviour behaviour)
    {
        if (result == CamRaycastResult.BPI && (BodyPartInformation)behaviour == m_CurrentBPI)
        {
            m_Timer = 0f;
        }
        else
        {
            // Looking Away
            m_Timer += Time.deltaTime;
            if (m_Timer >= m_LookAwayTime)
            {
                if (LookAwayEvent != null)
                    LookAwayEvent();
            }
        }
    }

    public override void Exit()
    {
        base.Exit();

        m_UIController.StopScrolling();
        m_UIController.HideUI();
    }
}
