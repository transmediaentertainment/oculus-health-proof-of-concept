﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SpeechSection
{
    public float Time;
    public string Text;
}

public class BodyPartInformation : MonoBehaviour 
{
    private static List<BodyPartInformation> m_BodyParts = new List<BodyPartInformation>();

    public string m_PartName;
    [Multiline]
    public string m_Origin;
    [Multiline]
    public string m_Insertion;
    [Multiline]
    public string m_Function;
    [Multiline]
    public string m_Innervation;

    private Material m_SharedMat;
    private Material m_MainMat;
    private Material m_OnTopMat;
    private bool m_IsHighlighted = false;

    private Color m_LowCol = Color.black;
    private Color m_HighCol = Color.white;
    private float m_LowRim = 0.75f;
    private float m_HighRim = 1.65f;

    public GameObject[] m_CombinedObjects;
    private Renderer[] m_RendererArray;

    public AudioClip m_Clip1;
    public AudioClip m_Clip2;
    public AudioClip m_Clip3;
    public AudioClip m_Clip4;

    public SpeechSection[] m_Clip1Sections;
    public SpeechSection[] m_Clip2Sections;
    public SpeechSection[] m_Clip3Sections;
    public SpeechSection[] m_Clip4Sections;

    void Awake()
    {
        m_BodyParts.Add(this);
        if (m_CombinedObjects == null || m_CombinedObjects.Length == 0)
        {
            // Use just our own components
            m_RendererArray = new Renderer[1];
            m_RendererArray[0] = GetComponent<Renderer>();
            var filter = GetComponent<MeshFilter>();
            var coll = gameObject.AddComponent<MeshCollider>();
            coll.sharedMesh = filter.mesh;
        }
        else
        {
            m_RendererArray = new Renderer[m_CombinedObjects.Length];
            for (int i = 0; i < m_CombinedObjects.Length; i++)
            {
                m_RendererArray[i] = m_CombinedObjects[i].GetComponent<Renderer>();
                var filter = m_CombinedObjects[i].GetComponent<MeshFilter>();
                var coll = gameObject.AddComponent<MeshCollider>();
                coll.sharedMesh = filter.mesh;
            }
        }

        m_SharedMat = m_RendererArray[0].sharedMaterial;
        // Doing this gives us a unique instance to work with
        m_MainMat = m_RendererArray[0].material;
        m_OnTopMat = new Material(m_RendererArray[0].material);
		m_OnTopMat.shader = Shader.Find("Shader Forge/BumpedSpecularRimlightOnTop");
        for (int i = 0; i < m_RendererArray.Length; i++)
        {
            m_RendererArray[i].sharedMaterial = m_MainMat;
        }
    }

    public void TurnOnHighlight()
    {
        m_IsHighlighted = true;
        for (int i = 0; i < m_RendererArray.Length; i++)
        {
            m_RendererArray[i].sharedMaterial = m_OnTopMat;
        }
        UIController.Instance.DimMainMaterials();
        // Turn off rim lighting on all other body parts
        for (int i = 0; i < m_BodyParts.Count; i++)
        {
            if (m_BodyParts[i] != this)
            {
                m_BodyParts[i].SetSharedMaterial();
            }
        }
    }

    public void TurnOffHighlight()
    {
        m_IsHighlighted = false;
        UIController.Instance.BrightenMainMainterials();
        for (int i = 0; i < m_BodyParts.Count; i++)
        {
            m_BodyParts[i].SetMainMaterial();
        }
    }

    void Update()
    {
        ModulateRim(m_MainMat);
        ModulateRim(m_OnTopMat);
        /*if (m_IsHighlighted)
        {
            var sin = Mathf.Sin(Time.time * 4f);
            var t = (sin + 1f) * 0.5f;
            m_OnTopMat.SetColor("_RimColor", Color.Lerp(m_LowCol, m_HighCol, t));
            m_OnTopMat.SetFloat("_RimPower", Mathf.Lerp(m_LowRim, m_HighRim, t));
        }*/
    }

    private void ModulateRim(Material mat)
    {
        var sin = Mathf.Sin(Time.time * 4f);
        var t = (sin + 1f) * 0.5f;
        mat.SetColor("_RimColor", Color.Lerp(m_LowCol, m_HighCol, t));
        mat.SetFloat("_RimPower", Mathf.Lerp(m_LowRim, m_HighRim, t));
    }

    private void SetSharedMaterial()
    {
        for (int i = 0; i < m_RendererArray.Length; i++)
        {
            m_RendererArray[i].sharedMaterial = m_SharedMat;
        }
    }

    private void SetMainMaterial()
    {
        for (int i = 0; i < m_RendererArray.Length; i++)
        {
            m_RendererArray[i].sharedMaterial = m_MainMat;
        }
    }
}
