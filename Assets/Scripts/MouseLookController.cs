﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class MouseLookController : MonoBehaviour {

	private MouseLook m_MouseLook = new MouseLook();

	public Camera m_Cam;

	void Awake() {
		m_MouseLook.Init (transform, m_Cam.transform);
	}

	// Update is called once per frame
	void Update () {
		m_MouseLook.LookRotation (transform, m_Cam.transform);
	}
}
