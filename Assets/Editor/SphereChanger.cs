﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SphereChanger
{
    [MenuItem("Tools/Regular Sphere Changer")]
    public static void ChangeRegularSpheres()
    {
        var prefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/PositioningSphere.prefab", typeof(GameObject));
        ChangeSpheres(prefab);
    }

    [MenuItem("Tools/Highlight Sphere Changer")]
    public static void ChangeHighlightSpheres()
    {
        var prefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/HighlightPositioningSphere.prefab", typeof(GameObject));
        ChangeSpheres(prefab);
    }

    private static void ChangeSpheres(GameObject prefab)
    {
        var ts = Selection.transforms;
        for (int i = 0; i < ts.Length; i++)
        {
            var newGO = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
            newGO.transform.position = ts[i].position;
            GameObject.DestroyImmediate(ts[i].gameObject, false);
            EditorUtility.SetDirty(newGO);
        }
        Debug.Log("Done");
    }
}
